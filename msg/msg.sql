-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2014 at 06:53 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `msg`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `firstname`, `lastname`, `email`, `telephone`) VALUES
(1, 'Munyaradzi Kenneth', 'Maposa', 'kenneth.maposa@multichoice.co.za', '0726413956'),
(3, 'Matsaka', 'Mashamaite', 'mtkmash27@gmail.com', ''),
(4, 'Mufaro', 'Maposa', 'maposamufaro@gmail.com', ''),
(5, 'Moipone', 'Mathe', 'mponybmathe@gmail.com', ''),
(6, 'Glory', 'Sello', 'glory.sello@gmail.com', ''),
(7, 'Pakiso', 'Mphatsoe', 'johnsonmphatsoe@gmail.com', ''),
(8, 'Boitumelo', 'Lempe', 'tumilempe@yahoo.co.uk', ''),
(9, 'Thandi', 'Radebe', 'radebe.thandi@gmail.com', ''),
(10, 'Kamohelo', 'Maduna', 'kamohelomaduna@gmail.com', ''),
(11, 'Rebbeca', 'Muller', 'rvmuller89@gmail.com', '0725467898'),
(12, 'Evelyn', 'Mackay', 'evelynmackay9@gmail.com', '0834567890');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
