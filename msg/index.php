<html>
<head>
	
	<link href="css/demo_table.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" language="javascript" src="js/jquery-1.7.2.min.js"></script>
	<script class="jsbin" src="js/jquery.dataTables.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#partners').dataTable({
				"aaSorting": [[ 1, "asc" ]]
			});
		});
	</script>
</head>

<body>
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
}
-->
</style>

<?php

	//$con = mysql_connect("localhost","root","P@ssw0rd"); // replace dbuser, dbpass with your db user and password
	//mysql_select_db("msg", $con);
	include 'database.php';
	$result = mysql_query("SELECT * FROM members") ; 	
?>

<div align="center">
<p class="style2"> MSG List of Partners </p>



	  <table cellpadding="0" cellspacing="0" border="0" class="display" id="partners">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>View</th>
            <th>Modify</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          <?php
            while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$id=$row["id"];
		  ?>
          <tr class="odd gradeX">		
            <td class="center"><?php echo $row["firstname"]; ?></td>
            <td class="center"><?php echo $row["lastname"]; ?></td>	
			<td class="center"><?php echo "<a href='viewPartner.php?id=$id'>View</a>" ?></td>
			<td class="center"><?php echo "<a href='modifyPartner.php?id=$id'>Edit</a>" ?></td>
			<td class="center"><?php echo "<a href='deletePartner.php?id=$id'>Delete</a>" ?></td>
						         
          </tr>
		  <?php 
			}
			mysql_free_result($result);
		  ?>
        </tbody>
      </table>
	  
	  
		 
	  <br/><a href="addPartner.html">Add New Partner</a> | <a href="sendMail.html">Send Bulk Email</a>
</div>
</body>
</html>